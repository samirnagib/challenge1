FROM nginx:latest

LABEL MAINTENER="Samir Nagib <samir.nagib@gmail.com>"
LABEL VERSION="1.0.0"

COPY ./site-content/* /usr/share/nginx/html/
